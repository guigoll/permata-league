class CreateTeams < ActiveRecord::Migration[5.1]
  def change
    create_table :teams do |t|
      t.string :name
      t.references :kind_team, foreign_key: true
      t.string :logo
      t.text :description
      t.integer :point
      t.references :user, foreign_key: true
      t.timestamps
    end
  end
end

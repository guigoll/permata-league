class CreateGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :groups do |t|
      t.references :round, foreign_key: true
      t.integer :amount_team

      t.timestamps
    end
  end
end

class CreateChampionshipKinds < ActiveRecord::Migration[5.1]
  def change
    create_table :championship_kinds do |t|
      t.string :description
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end

class CreateChampionshipConfigurations < ActiveRecord::Migration[5.1]
  def change
    create_table :championship_configurations do |t|
      t.integer :round
      t.references :championship_kind, foreign_key: true
      t.integer :amount_team

      t.timestamps
    end
  end
end

class CreateChampionships < ActiveRecord::Migration[5.1]
  def change
    create_table :championships do |t|
      t.string :description
      t.integer :teams_count
      t.references :championship_kind, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end

class CreateRounds < ActiveRecord::Migration[5.1]
  def change
    create_table :rounds do |t|
      t.string :description
      t.references :championship_configuration, foreign_key: true
      t.integer :amount_group

      t.timestamps
    end
  end
end

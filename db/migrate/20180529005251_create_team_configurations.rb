class CreateTeamConfigurations < ActiveRecord::Migration[5.1]
  def change
    create_table :team_configurations do |t|
      t.references :team, foreign_key: true
      t.string :mascot
      t.string :shirt

      t.timestamps
    end
  end
end

class Group < ApplicationRecord
  belongs_to :round, optional: true

  validates_presence_of :description , :round_id, :amount_team
end

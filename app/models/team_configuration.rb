class TeamConfiguration < ApplicationRecord
  belongs_to :team

  validates_presence_of :team_id
end

class KindTeam < ApplicationRecord
  has_many :team
  validates_presence_of :description

end

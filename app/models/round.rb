class Round < ApplicationRecord
  belongs_to :championship_configuration, optional: true
  has_many :groups,  dependent: :destroy

  validates_presence_of :description , :championship_configuration_id, :amount_group
  accepts_nested_attributes_for :groups, allow_destroy: true
end

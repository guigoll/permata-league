class ChampionshipKind < ApplicationRecord
  belongs_to :user
  has_many :championships
  has_many :championship_configurations, dependent: :destroy

  validates_presence_of :description , :user_id
  accepts_nested_attributes_for :championship_configurations, allow_destroy: true
end

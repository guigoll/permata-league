class Kind < ApplicationRecord
  has_one :user
  validates_presence_of :description
end

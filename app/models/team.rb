class Team < ApplicationRecord
  belongs_to :kind_team
  belongs_to :user
  belongs_to :team_configuration
  has_many :memberships
  validates_presence_of :name, :user_id, :kind_team_id

  accepts_nested_attributes_for :memberships, allow_destroy: true
end

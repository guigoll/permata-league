class ChampionshipConfiguration < ApplicationRecord
  belongs_to :championship_kind, optional: true
  has_many :rounds, dependent: :destroy

  validates_presence_of :round, :amount_team
  accepts_nested_attributes_for :rounds, allow_destroy: true
end

class Position < ApplicationRecord
  belongs_to :kind_team

  validates_presence_of :description, :kind_team_id
end

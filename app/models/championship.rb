class Championship < ApplicationRecord
  belongs_to :championship_kind
  belongs_to :user

  validates_presence_of :description, :championship_kind_id, :user_id
end

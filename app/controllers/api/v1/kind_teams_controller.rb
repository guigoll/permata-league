class Api::V1::KindTeamsController < ApplicationController
  before_action :authenticate_user!

  def index
    kind_teams =  KindTeam.all
    # byebug
    render json: {kind_team: kind_teams}, status: 200
  end

  def show
    kind_team = KindTeam.find(params[:id])
    render json: kind_team, status: 200
  end

  def create
    kind_team = KindTeam.new(kind_team_params)
    if kind_team.save
        render json: kind_team, status: 201
    else
        render json: { errors: kind_team.errors}, status: 422
    end
  end

  private

  def kind_team_params
    params.require(:kind_team).permit(:description)
  end
end

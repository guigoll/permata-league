class Api::V1::TeamsController < ApplicationController
  before_action :authenticate_user!

  def index
    teams = current_user.teams.all
    # byebug
    render json: {team: teams}, status: 200
  end

  def show
    team  = current_user.teams.find(params[:id])
    # byebug
    render json: team, status: 200
  end

  def create
    team = current_user.teams.build(team_params)
    # byebug
    if team.save
      render json: team, status: 201
    else
      render json: {errors: team.errors}, status: 422
    end
  end

  def update
    team = current_user.teams.find(params[:id])
    # byebug
    if team.update_attributes(team_params)
      render json: team, status: 200
    else
      render json: { errors: team.errors }, status: 422
    end
  end

  private
  def team_params
    params.require(:team).permit(:name,:kind_team_id, :logo, :description, :point, :user_id,
      memberships_attributes:[:id,:user_id, :team_id])
    end

  end

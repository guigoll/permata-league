class Api::V1::ChampionshipsController < ApplicationController
  before_action :authenticate_user!
  def index
    championships = current_user.championships.all
    render json: { championship: championships }, status: 200
  end

  def show
    championship = current_user.championships.find(params[:id])
    render json: championship, status: 200
  end

  def create
     championship = current_user.championships.build(championship_params)
     if championship.save
        render json: championship, status: 201
      else
        render json: { errors: championship.errors }, status: 422
     end
  end

  def update
    championship = current_user.championships.find(params[:id])
    # byebug
    if championship.update_attributes(championship_params)
      # byebug
       render json: championship, status:200
     else
       render json: { errors: championship.errors}, status: 422
    end
  end

  def championship_params
    params.require(:championship).permit(:description, :user_id, :championship_kind_id)        
  end

end

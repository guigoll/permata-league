class Api::V1::ChampionshipKindsController < ApplicationController
  before_action :authenticate_user!

  def index
    championship_kinds = current_user.championship_kinds.all
     # byebug
    render json: {championship_kind:  championship_kinds}, status: 200
  end

  def show
    championship_kind = current_user.championship_kinds.find(params[:id])
    # byebug
    render json: championship_kind, status: 200
  end

  def create
    championship_kind = current_user.championship_kinds.build(championship_kind_params)

    if championship_kind.save
       render json: championship_kind, status: 201
     else
       render json: {errors: championship_kind.errors }, status: 422
    end
  end

  def update
     championship_kind = current_user.championship_kinds.find(params[:id])
      # byebug
     if championship_kind.update_attributes(championship_kind_params)
        render json: championship_kind, status: 200
        # byebug
      else
        render json: { errors: championship_kind.errors}, status: 422
     end
  end

  private
   def championship_kind_params
     params.require(:championship_kind).permit(:description,
                :championship_configurations_attributes => [:round, :amount_team,
                 :rounds_attributes =>[:description, :amount_group, :groups_attributes =>[:description, :amount_team]]])
   end
end

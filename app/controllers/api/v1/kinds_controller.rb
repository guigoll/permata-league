class Api::V1::KindsController < ApplicationController
 before_action :authenticate_user!

  def index
    kind = Kind.all
    # byebug
    render json: {kind: kind}, status: 200
  end

  def show
    kind = Kind.find(params[:id])
    render json: kind, status: 200
  end

  def create
    kind = Kind.create(kind_params)
    if kind.save
      render json: kind, status: 201
    else
      render json:{ errors: kind.errors}, status: 422
    end
  end

  def update
    kind = Kind.find(params[:id])

    if kind.update_attributes(kind_params)
      render json: kind, status: 200
    else
      render json: {errors: kind.errors}, status: 422
    end
  end

  private

  def kind_params
    params.require(:kind).permit(:description)
  end

end

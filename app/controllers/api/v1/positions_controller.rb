class Api::V1::PositionsController < ApplicationController
  before_action :authenticate_user!
  def index
    positions = Position.all
    render json:{position: positions}, status: 200
  end

  def show
    position = Position.find(params[:id])
    render json: position, status: 200
  end

  def create
    position = Position.create(position_params)
    # byebug
    if position.save
      render json: position, status: 201
    else
      render json:{errors: position.errors}, status: 422
    end
  end

  def update
    position = Position.find(params[:id])
    # byebug
    if position.update_attributes(position_params)
      render json: position, status: 200
    end
  end

  private
  def position_params
    params.require(:position).permit(:description, :kind_team_id)
  end

end

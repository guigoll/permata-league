require 'api_version_constraint'

Rails.application.routes.draw do

  # Mapear as sessões que foram solicitadas
  devise_for :users, only: [:sessions], controller: {sessions: 'api/v1/sessions'}

   namespace :api, default:{ format: :json}, constrainsts: {subdomain: 'api'}, path: '/' do

       namespace :v1, path: '/', constraints: ApiVersionConstraint.new(version: 1, default: true) do
         mount_devise_token_auth_for 'User', at: 'auth'
         resources :kinds, only:[:index, :show, :create, :update]
         resources :users, only:[:index, :show, :create]
         resources :sessions, only:[:create]
         resources :kind_teams, only:[:index, :show, :create]
         resources :teams, only:[:index, :show, :create, :update]
         resources :positions, only:[:index, :show, :create, :update]
         resources :championship_kinds, only:[:index, :show, :create, :update]
         resources :championships, only:[:index, :show, :create, :update]
       end

   end
end

require 'rails_helper'

RSpec.describe Round, type: :model do
 let(:round){ FactoryBot.build(:round)}

 # verifica os relacionamentos
 it{ is_expected.to belong_to(:championship_configuration)}
 it{ is_expected.to have_many(:groups)}

 # verifica os campos obrigatorios a ser preenchidos
 it{ is_expected.to validate_presence_of :championship_configuration_id}
 it{ is_expected.to validate_presence_of :description }
 it{ is_expected.to validate_presence_of :amount_group}

 # verifica se os campos ainda existe no database
 it{ is_expected.to respond_to(:description)}
 it{ is_expected.to respond_to(:amount_group)}
 it{ is_expected.to respond_to(:championship_configuration_id)}

end

require 'rails_helper'

RSpec.describe ChampionshipKind, type: :model do
   let(:championship_kind){ FactoryBot.build(:championship_kind)}

   # verifica os relacionamentos
   it{ is_expected.to belong_to(:user)}
   it{ is_expected.to have_many(:championship_configurations).dependent(:destroy)}

   # verifica os campos obrigatorios a ser preenchidos
   it{ is_expected.to validate_presence_of :user_id}
   it{ is_expected.to validate_presence_of :description }

   # verifica se os campos ainda existe no database
   it{ is_expected.to respond_to(:description)}

end

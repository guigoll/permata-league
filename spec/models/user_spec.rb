require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user){ FactoryBot.build(:user) }

  it{ is_expected.to belong_to(:kind)}

  # it{ is_expected.to validate_presence_of :name}
  # it{ is_expected.to validate_presence_of :email}
  # it{ is_expected.to validate_presence_of :kind_id}

  # Verifica se o campo existe
  it{ is_expected.to respond_to(:name) }
  it{ is_expected.to respond_to(:email) }
  it{ is_expected.to respond_to(:password) }
  it{ is_expected.to respond_to(:password_confirmation) }

end

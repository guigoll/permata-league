require 'rails_helper'

RSpec.describe Championship, type: :model do
  let(:championship){ FactoryBot.build(:championship)}

  # verifica os relacionamentos
  it{ is_expected.to belong_to(:user)}
  it{ is_expected.to belong_to(:championship_kind)}

  # verifica os campos obrigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :user_id}
  it{ is_expected.to validate_presence_of :championship_kind_id }

  # verifica se os campos ainda existe no database
  it{ is_expected.to respond_to(:description)}

end

require 'rails_helper'

RSpec.describe Group, type: :model do
  let(:group){ FactoryBot.build(:group)}

  # verifica os relacionamentos
  it{ is_expected.to belong_to(:round)}

  # verifica os campos obrigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :round_id}
  it{ is_expected.to validate_presence_of :description }
  it{ is_expected.to validate_presence_of :amount_team}

  # verifica se os campos ainda existe no database
  it{ is_expected.to respond_to(:description)}
  it{ is_expected.to respond_to(:amount_team)}
end

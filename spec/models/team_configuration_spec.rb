require 'rails_helper'

RSpec.describe TeamConfiguration, type: :model do
  let(:team_configuration){ FactoryBot.build(:team_configuration)}

  # verifica os relacionamentos
  it{ is_expected.to belong_to(:team)}

  # verifica os campos obrigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :team_id}


  # verifica se os campos ainda existe no database
  it{ is_expected.to respond_to(:team_id)}
  it{ is_expected.to respond_to(:mascot)}
  it{ is_expected.to respond_to(:shirt)}
end

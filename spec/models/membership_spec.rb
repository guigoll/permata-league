require 'rails_helper'

RSpec.describe Membership, type: :model do
  # verifica os relacionamentos
  it{ is_expected.to belong_to(:user)}
  it{ is_expected.to belong_to(:team)}
end

require 'rails_helper'

RSpec.describe Team, type: :model do
  let(:team){ FactoryBot.build(:team) }

  # verifica os relacionamentos
  it{ is_expected.to belong_to(:user)}
  it{ is_expected.to belong_to(:kind_team)}

  # verifica os campos obrigatorios a ser preenchidos
  it{ is_expected.to validate_presence_of :user_id}
  it{ is_expected.to validate_presence_of :kind_team_id }
  it{ is_expected.to validate_presence_of :name }

  # verifica se os campos ainda existe no database
  it{ is_expected.to respond_to(:name)}
  it{ is_expected.to respond_to(:logo)}
  it{ is_expected.to respond_to(:description)}
  it{ is_expected.to respond_to(:point)}
end

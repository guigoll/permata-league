require 'rails_helper'

RSpec.describe Position, type: :model do

  let(:position){ FactoryBot.build(:position)}

  it{ is_expected.to belong_to(:kind_team)}

  it{ is_expected.to validate_presence_of :description }

  # verifica se os campos ainda existe no database
  it{ is_expected.to respond_to(:description)}
end

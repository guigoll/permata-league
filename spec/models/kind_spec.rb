require 'rails_helper'

RSpec.describe Kind, type: :model do

  let(:kind){ FactoryBot.build(:kind) }
  # Verifica se o campo existe
  it{ is_expected.to respond_to(:description) }

end

require 'rails_helper'

RSpec.describe "API   Championship Kinds", type: :request do

  before { host! 'api.permata.test'}

  let(:championship_kind){ FactoryBot.create(:championship_kind)}
  let(:user){ FactoryBot.create(:user)}
  let(:auth_data){ user.create_new_auth_token}

  let(:headers) do
    {
      'Accept' => 'application/vnd.permata.v1',
      'Content-type' => Mime[:json].to_s,
      'Access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end

  describe 'GET /championship_kinds' do
    before do
      create_list(:championship_kind, 5, user_id: user.id)
      get '/championship_kinds', params: {}, headers: headers
    end

    it 'return status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'return 5 championship_kind' do
      expect(json_body[:championship_kind,].count).to eq(5)
    end
  end

  describe 'GET /championship_kinds/:id' do
    let(:championship_kind){ FactoryBot.create(:championship_kind, user_id: user.id)}
    before{ get "/championship_kinds/#{championship_kind.id}", params: {}, headers: headers}

    it 'return status code 200' do
      expect(response).to  have_http_status(200)
    end

    it 'return the json for championship kinds ' do
      expect(json_body[:description]).to eq(championship_kind[:description])
    end
  end

  describe 'POST /championship_kinds' do
    before do
      post '/championship_kinds', params: { championship_kind: championship_kind_params}.to_json, headers: headers
    end

    context 'when the params are valid' do
      let(:championship_kind_params){ FactoryBot.create(:championship_kind, user_id: user.id)}

      it 'return status code 201' do
        expect(response).to have_http_status(201)
      end

      it 'save championsship kind in database' do
        expect(ChampionshipKind.find_by(description: championship_kind_params[:description])).not_to be_nil
      end

      it 'return the json for create championship kind' do
        expect(json_body[:description]).to eq(championship_kind_params[:description])
      end

      it 'return the current user' do
        expect(json_body[:user_id]).to eq(championship_kind_params[:user_id])
      end
    end

    context 'when the params are invalid' do
      let(:championship_kind_params){ FactoryBot.attributes_for(:championship_kind,user_id: user.id, description: '')}

      it 'return status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'does not save in database' do
        expect(ChampionshipKind.find_by(description: championship_kind_params[:description])).to be_nil
      end

      it 'return the json errors' do
        expect(json_body[:errors]).to have_key(:description)
      end
    end
  end

  describe 'PUT /championship_kinds/:id' do
    let!(:championship_kind){ FactoryBot.create(:championship_kind, user_id: user.id)}
    before do
      put "/championship_kinds/#{championship_kind.id}", params: { championship_kind: championship_kind_params}.to_json, headers: headers
    end

    context 'when the params are valid' do
      let(:championship_kind_params){{ description: 'campeonato do Gui'}}
      it 'return status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'return the json championship kind' do
        expect(json_body[:description]).to eq(championship_kind_params[:description])
      end

      it 'return the update championship kind' do
        expect(ChampionshipKind.find_by(description: championship_kind_params[:description])).not_to be_nil
      end

    end

    context 'when the params are invalid' do
      let(:championship_kind_params){{ description: '' }}

      it 'return status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'return the json errors for championship kind' do
        expect(json_body[:errors]).to have_key(:description)
      end

      it 'return the update championship kind' do
        expect(ChampionshipKind.find_by(description: championship_kind_params[:description])).to be_nil
      end
    end
  end

  describe 'PUT /championship_kinds/:id add attributes configuration' do
    let(:championship_kind){ FactoryBot.create(:championship_kind, user_id:user.id)}

    before do
      put "/championship_kinds/#{championship_kind.id}", params:{championship_kind: championship_kind_params}.to_json, headers: headers
    end

    context 'when the attributes are valid' do
      let(:championship_kind_params){ FactoryBot.create(:championship_kind, :with_configuration)}
      it 'save championship configuration' do
        expect(ChampionshipKind.last.championship_configurations.find_by(championship_kind_id: championship_kind_params[:id])).not_to be_nil
      end

      # it 'save rounds' do
      #    expect(ChampionshipKind.last.championship_configurations.last.rounds.find_by(round_id: championship_kind_params[]))
      # end
    end
  end

end

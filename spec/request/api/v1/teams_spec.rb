require 'rails_helper'

RSpec.describe 'API Teams', type: :request do

  before { host! 'api.permata.test'}

  let!(:kind_team){ FactoryBot.create(:kind_team)}
  let!(:user){ FactoryBot.create(:user)}
  let(:auth_data){ user.create_new_auth_token }

  let(:headers) do
    {
      'Accept' => 'application/vnd.permata.v1',
      'Content-type' => Mime[:json].to_s,
      'Access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end


  describe 'GET /teams' do
    before do
      FactoryBot.create_list(:team, 5, user_id: user.id, kind_team_id: kind_team.id)
      get '/teams', params:{}, headers: headers
    end

    it 'return status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'return 5 teams' do
      expect(json_body[:team].count).to eq(5)
    end
  end

  describe 'GET /teams/:id' do
    let(:team){ FactoryBot.create(:team, user_id: user.id, kind_team_id: kind_team.id)}
    before{ get "/teams/#{team.id}", params: {}, headers: headers}

    it 'return status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'return the json for the team' do
      expect(json_body[:name]).to eq(team.name)
    end

  end

  describe 'POST /teams' do
    before do
      post '/teams', params: {team: team_params}.to_json, headers: headers
    end

    context "when the params are valid" do
      let(:team_params){ FactoryBot.create(:team, user_id: user.id)}

      it 'return status code 201' do
        expect(response).to have_http_status(201)
      end

      it 'save the team in the database' do
        expect(Team.find_by(name: team_params[:name])).not_to be_nil
      end

      it 'return the json for create team' do
        expect(json_body[:name]).to eq(team_params[:name])
      end

      it 'return the create team to the current user' do
        expect(json_body[:user_id]).to eq(team_params[:user_id])
      end

    end

    context "when  the params are invalid" do
      let(:team_params){ FactoryBot.build(:team, user_id: user.id, name: '')}

      it 'return status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'does not save in the database' do
        expect(Team.find_by(name: team_params[:name])).to be_nil
      end

      it 'return the json error for name' do
        expect(json_body[:errors]).to have_key(:name)
      end
    end
  end

  describe 'PUT /teams/:id' do
    let!(:team){ FactoryBot.create(:team, user_id: user.id, kind_team_id: kind_team.id)}
    before do
      put "/teams/#{team.id}",params: { team: team_params }.to_json, headers: headers
    end

    context "when  the params are valid" do
      let(:team_params){ { name: 'Time do Gui' }}

      it 'return status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'return json for update team' do
        expect(json_body[:name]).to eq(team_params[:name])
      end

      it 'update team in the database' do
        expect(Team.find_by(name: team_params[:name])).not_to be_nil
      end

    end
  end

  describe 'PUT /teams/:id add attributes' do
    let(:team){ FactoryBot.create(:team, user_id:user.id)}

    before do
      put "/teams/#{team.id}", params:{team: team_params}.to_json, headers: headers
    end

    context 'when the attributes are valid' do
      let(:team_params){ FactoryBot.create(:team, :with_membership)}
      it 'save team membership' do
        expect(Team.last.memberships.find_by(team_id: team_params[:id])).not_to be_nil
      end
    end
  end
end

require 'rails_helper'

RSpec.describe 'Kind Teams API', type: :request do
  
  before { host! 'api.permata.test'}

  let!(:kind_team){ FactoryBot.create(:kind_team)}
  let(:user){ FactoryBot.create(:user)}
  let(:auth_data){ user.create_new_auth_token}

  let(:headers) do
    {
      'Accept' => 'application/vnd.permata.v1',
      'Content-type' => Mime[:json].to_s,
      'Access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end

  describe 'GET /kind teams' do

    before do
      create_list(:kind_team, 5)
      get '/kind_teams', params:{}, headers: headers
    end

    it 'return status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'return 5 kind from databases' do
      expect(json_body[:kind_team].count).to eq(6)
    end
  end

  describe 'GET /kind_team/:id' do
    let(:kind_team){ FactoryBot.create(:kind_team)}

    before {get "/kind_teams/#{kind_team.id}", params: {}, headers: headers}

    it 'return status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'return the json for kind team' do
      expect(json_body[:description]).to eq(kind_team.description)
    end

  end

  describe 'POST /kind_teams' do
    before do
      post '/kind_teams', params:{ kind_team: kind_team_params}.to_json, headers: headers
    end

    context 'when the params are valid' do
      let(:kind_team_params){ FactoryBot.attributes_for(:kind_team)}
      it 'return status code 201' do
        expect(response).to have_http_status(201)
      end

     it 'save the kind team in the database' do
        expect(KindTeam.find_by(description: kind_team_params[:description])).not_to be_nil
     end

     it 'return json for create kind team' do
       expect(json_body[:description]).to eq(kind_team_params[:description])
     end
    end

    context 'when the params are invalid' do
      let(:kind_team_params){ FactoryBot.attributes_for(:kind_team, description: '')}

      it 'return status code 422' do
         expect(response).to have_http_status(422)
      end

      it 'when the kind team not save in the database' do
         expect(KindTeam.find_by(description: kind_team_params[:description])).to be_nil
      end

      it 'return error for the kind teamn description' do
        expect(json_body[:errors]).to  have_key(:description)
      end
    end

  end
end

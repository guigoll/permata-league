require 'rails_helper'

RSpec.describe 'API Championship', type: :request do
  before { host! 'api.permata.test' }

  let(:championship_kind){ FactoryBot.create(:championship_kind)}
  let(:user){ FactoryBot.create(:user)}
  let(:auth_data){ user.create_new_auth_token}
  let(:headers) do
    {
      'Accept' => 'application/vnd.permata.v1',
      'Content-type' => Mime[:json].to_s,
      'Access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end
  describe 'GET /championships' do
    before do
      FactoryBot.create_list(:championship, 5, user_id: user.id, championship_kind_id: championship_kind.id)
      get '/championships', params: {}, headers: headers
    end

    it 'return status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'return 5 championship' do
      expect(json_body[:championship].count).to eq(5)
    end

  end

  describe 'GET /championships/id' do
    let(:championship){ FactoryBot.create(:championship, user_id: user.id, championship_kind_id: championship_kind.id)}
    before{ get "/championships/#{championship.id}", params: {}, headers: headers}

    it 'return status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'return json for championship' do
      expect(json_body[:description]).to eq(championship.description)
    end
  end

  describe 'POST /championships' do
    before do
      post '/championships', params: { championship: championship_params}.to_json, headers: headers
    end

    context 'when the params are valid' do
      let(:championship_params){ FactoryBot.create(:championship, user_id: user.id, championship_kind_id: championship_kind.id)}
      it 'return status code 201' do
        expect(response).to have_http_status(201)
      end

      it 'return json for championship' do
        expect(json_body[:description]).to eq(championship_params[:description])
      end

      it 'save championship in database' do
        expect(Championship.find_by(description: championship_params[:description])).not_to be_nil
      end
    end
    context 'when the params are invalid' do
      let(:championship_params){ FactoryBot.build(:championship, user_id: user.id, championship_kind_id: championship_kind.id, description: '')}

      it 'return status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'return json error for description' do
        expect(json_body[:errors]).to have_key(:description)
      end

      it 'not save in database' do
        expect(Championship.find_by(description: championship_params[:description])).to be_nil
      end
    end
  end

  describe 'PUT /championships/:id' do
    let!(:championship){ FactoryBot.create(:championship, user_id: user.id, championship_kind_id: championship_kind.id)}
    before do
      put "/championships/#{championship.id}", params:{ championship: championship_params}.to_json, headers: headers
    end

    context 'when the params are valid' do
      let(:championship_params){{ description: 'Competição do Gui'}}

      it 'return status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'return json update championship' do
        expect(json_body[:description]).to eq(championship_params[:description])
      end

      it 'update championship in database' do
        expect(Championship.find_by(description: championship_params[:description])).not_to be_nil
      end
    end

    context 'when the params are invalid' do
      let(:championship_params){{ description: ' ' }}

      it 'return status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'return json error for championship' do
        expect(json_body[:errors]).to have_key(:description)
      end

      it 'not update in database' do
        expect(Championship.find_by(description: championship_params[:description])).to be_nil
      end
    end
  end

end

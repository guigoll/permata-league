require 'rails_helper'

RSpec.describe 'API Positions', type: :request do

  before { host! 'api.permata.test'}
  let(:kind_team){ FactoryBot.create(:kind_team)}
  let(:user){ FactoryBot.create(:user)}
  let(:auth_data){ user.create_new_auth_token }

  let(:headers) do
    {
      'Accept' => 'application/vnd.permata.v1',
      'Content-type' => Mime[:json].to_s,
      'Access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end

  describe 'GET /postionss' do
    before do
      create_list(:position, 5)
      get '/positions', params:{}, headers: headers
    end

    it 'return status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'return 5 positon' do
      expect(json_body[:position].count).to eq (5)
    end
  end

  describe 'GET /positions/:id' do
    let(:position){ FactoryBot.create(:position)}
    before{ get "/positions/#{position.id}", params:{}, headers: headers }

    it 'return status code 200' do
      expect(response).to have_http_status(200)
    end
    it 'return the json for de position' do
      expect(json_body[:description]).to eq(position.description)
    end

  end

  describe 'POST /positions' do
    before do
      post '/positions', params: {position: position_params }.to_json, headers: headers
    end

    context 'when the params are valid' do
      let(:position_params){ FactoryBot.build(:position, kind_team_id: kind_team.id)}

      it "return status code 201" do
        expect(response).to have_http_status(201)
      end

      it 'save the position in database' do
        expect(Position.find_by(description: position_params[:description])).not_to be_nil
      end

      it 'return the json create position' do
        expect(json_body[:description]).to eq(position_params[:description])
      end
    end

    context "when the params are invalid" do
      let(:position_params){ FactoryBot.build(:position, description: '')}

      it 'return status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'when the positon not save in database' do
        expect(Position.find_by(description: position_params[:description])).to be_nil
      end


      it 'return the json for the error' do
        expect(json_body).to have_key(:errors)
      end
    end
  end

  describe 'PUT /positions/:id' do
    let!(:position){ FactoryBot.create(:position, kind_team_id: kind_team.id)}
      before do
        put "/positions/#{position.id}", params: {position: position_params}.to_json, headers: headers
      end

      context 'when the params are valid' do
         let(:position_params){ { description: 'Lateral Esquerdo' }}

         it 'return status code 200' do
            expect(response).to have_http_status(200)
         end

         it 'return json update for position' do
           expect(json_body[:description]).to eq(position_params[:description])
         end

         it 'update the positon in database' do
           expect(Position.find_by(description: position_params[:description])).not_to be_nil
         end
      end
  end

end

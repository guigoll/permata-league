require 'rails_helper'

RSpec.describe 'Kind API', type: :request do
  before { host! 'api.permata.test'}

  let!(:kind){ FactoryBot.create(:kind)}
  let(:user){ FactoryBot.create(:user)}
  let(:auth_data){ user.create_new_auth_token }
  let(:headers) do
    {
        'Accept' => 'application/vnd.permata.v1',
        'Content-type' => Mime[:json].to_s,
        'Access-token' => auth_data['access-token'],
        'uid' => auth_data['uid'],
        'client' => auth_data['client']
    }
  end

  describe 'GET /Kinds' do
    before do
      create_list(:kind, 4)
      get '/kinds', params: {}, headers: headers
    end

    it 'return status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'return 5 kind from database' do
      expect(json_body[:kind].count).to eq(6)
    end

  end

  describe 'GET /kinds/:id' do
    let(:kind){ create(:kind) }

    before{ get "/kinds/#{kind.id}", params: {}, headers: headers }

    it 'return status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'return the json for kind' do
      expect(json_body[:description]).to eq(kind.description)
    end
  end

  describe 'POST /kinds' do
    before do
      post '/kinds', params: {kind: kind_params}.to_json, headers: headers
    end

    context 'when the params are valid' do
      let(:kind_params){ attributes_for(:kind)}

      it 'return status code 201' do
        expect(response).to have_http_status(201)
      end

      it 'save the kinds in the database' do
        expect(Kind.find_by(description: kind_params[:description])).not_to be_nil
      end

      it 'return json for create kind' do
        expect(json_body[:description]).to eq(kind_params[:description])
      end
    end

    context 'when the params not are valid' do
      let(:kind_params){ attributes_for(:kind, description: '')}

      it 'return status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'does not save the task in the database' do
        expect(Kind.find_by(description: kind_params[:description])).to be_nil
      end

      it 'return the json error for title' do
         expect(json_body).to have_key(:errors)
      end
    end
  end

  describe 'PUT /kind/:id' do
    let!(:kind){ create(:kind)}
    before do
      put "/kinds/#{kind.id}", params: {kind: kind_params}.to_json, headers: headers
    end

    context 'when the params are valid' do
      let(:kind_params){{ description: 'Teste de atualização' }}

      it 'return status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'return json for update task' do
        expect(json_body[:description]).to eq(kind_params[:description])
      end

      it 'update the task in the database' do
        expect(Kind.find_by(description: kind_params[:description])).not_to be_nil
      end

    end

    context 'when the params  are invalid' do
      let(:kind_params){{ description: '' }}

      it 'return status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'return the json error for description' do
           expect(json_body).to have_key(:errors)
      end

      it 'does not update the kind in the database' do
        expect(Kind.find_by(description: kind_params[:description])).to be_nil
      end
    end
  end
end

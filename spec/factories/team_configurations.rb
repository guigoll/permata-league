FactoryBot.define do
  factory :team_configuration do
    team
    mascot { Faker::Team.mascot }
    shirt {Faker::File.file_name('path/to')}
  end
end

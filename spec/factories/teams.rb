FactoryBot.define do
  factory :team do
    name { Faker::Football.team }
    kind_team
    user
    logo { Faker::LoremPixel.image("50x60", false, 'sports') }
    description { Faker::Lorem.sentence(3)}
    point 1

    trait :with_membership do
      after(:create) do |team|
        create(:membership, team: team)
      end
    end
  end
end

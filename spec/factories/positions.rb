FactoryBot.define do
  factory :position do
    description {Faker::Job.position}
    kind_team
  end
end

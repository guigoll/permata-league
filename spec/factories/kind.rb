FactoryBot.define do
   factory :kind do
       description { Faker::Job.title }
   end
end

FactoryBot.define do
  factory :group do
    sequence(:description, 'A') {|n| "Grupo #{n}" }
    round
    amount_team 5
  end
end

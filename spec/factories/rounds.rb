FactoryBot.define do
  factory :round do
    sequence(:description) {|n| "Fase #{n}" }
    championship_configuration
    amount_group 4

    factory :rounds_with_group do
      after(:create) do |round|
         create(:group, round: round)
      end
    end
  end
end

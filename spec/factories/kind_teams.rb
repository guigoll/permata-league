FactoryBot.define do
  factory :kind_team do
    description { Faker::Team.sport }
  end
end

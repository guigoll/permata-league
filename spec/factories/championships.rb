FactoryBot.define do
  factory :championship do
    description { Faker::Football.competition }
    teams_count 10
    championship_kind
    user
  end
end

FactoryBot.define do
  factory :championship_configuration do
    round 1
    championship_kind
    amount_team 20


    factory :championship_configuration_round do
      after(:create) do |championship_configuration|
        create(:rounds_with_group,championship_configuration: championship_configuration)
      end
    end
  end
end

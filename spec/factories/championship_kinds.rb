FactoryBot.define do
  factory :championship_kind do
    description { Faker::Team.sport }
    user
  end

  # Agrupar atributos para forma novas factories
  trait :with_configuration do
    after(:create) do |championship_kind|
      create(:championship_configuration_round, championship_kind: championship_kind)
    end
  end
end
